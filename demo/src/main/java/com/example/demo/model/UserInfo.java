package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

//自动生成get/set
@Data
@Table(name = "tb_user")
@Entity
public class UserInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column
    String nikeName;

    @Column
    String userName;

    @Column
    String sex;

    public UserInfo() {
    }

    public UserInfo(String nikeName, String userName, String sex) {
        this.nikeName = nikeName;
        this.userName = userName;
        this.sex = sex;
    }
}
