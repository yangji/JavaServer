package com.example.demo.server;


import com.example.demo.model.UserInfo;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//声明服务
@Service
public class UserServer {

    @Autowired
    UserRepository repository;

    public UserInfo login(String name, String pwd) {
        if ("admin".equals(name) && "123456".equals(pwd)) {
            UserInfo info = new UserInfo("张三", name, "男");
            return info;
        } else {
            return null;
        }
    }

    public void re() {
        UserInfo info = new UserInfo("李四", "admin123", "男");
        repository.save(info);
    }

}
