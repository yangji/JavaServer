package com.example.demo.controller;


import com.example.demo.model.UserInfo;
import com.example.demo.server.UserServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserServer server;

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(@RequestParam(name = "userName", required = true) String name, @RequestParam(name = "userPwd", required = true) String pwd) {
        UserInfo info = server.login(name, pwd);
        if (info != null) {
            return "登录成功！";
        } else {
            return "登录失败";
        }
    }


    @RequestMapping(value = "re", method = RequestMethod.GET)
    public String re() {
        server.re();
        return "注册成功";
    }

}
